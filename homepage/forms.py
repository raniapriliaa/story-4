from django import forms
from .models import Jadwal

class JadwalForm(forms.ModelForm):

    kegiatan = forms.CharField(widget=forms.TextInput(attrs={
        "class" : "form-control",
        "placeholder" : "Nama Kegiatan",
    }))
    hari = forms.CharField(widget=forms.TextInput(attrs={
        "class" : "form-control",
        "placeholder" : "Hari",
    }))
    tanggal = forms.DateField(widget=forms.DateInput(attrs={
        "class" : "form-control",
        "placeholder" : "(YYYY-MM-DD)"
    }))
    jam = forms.TimeField(widget=forms.TimeInput(attrs={
        "class" : "form-control",
        "placeholder" : "(JAM:MENIT:DETIK)"
    }))
    tempat = forms.CharField(widget=forms.TextInput(attrs={
        "class" : "form-control",
        "placeholder" : "Nama Tempat",
    }))
    kategori = forms.CharField(widget=forms.TextInput(attrs={
        "class" : "form-control",
        "placeholder" : "Kategori",
    }))

    class Meta:
        model = Jadwal
        fields = ['kegiatan', 'tanggal', 'jam', 'tempat', 'kategori', 'hari']
        

