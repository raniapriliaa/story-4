from django.urls import path
from . import views

app_name = 'homepage'

urlpatterns = [
    path('', views.home1, name='home1'),
    path('about/', views.about, name='about'),
    path('education/', views.education, name='education'),
    path('experience/', views.experience, name='experience'),
    path('schedule/', views.jadwal, name='jadwal'),
    path('schedule_create/', views.schedule_create, name='schedule_create'),
    path('schedule_delete/', views.schedule_delete, name='schedule_delete'),
]
