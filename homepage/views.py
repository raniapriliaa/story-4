from django.shortcuts import render, redirect
from django.http import HttpResponse
from .models import Jadwal
from . import forms
 

# Create your views here.
def home1(request):
    return render(request, 'home1.html')

def about(request):
    return render(request, 'about.html')

def education(request):
    return render(request, 'education.html')

def experience(request):
    return render(request, 'experience.html')

def jadwal(request):
    jadwals = Jadwal.objects.order_by('tanggal')
    return render(request, 'schedule.html', {'jadwals': jadwals})

def schedule_create(request):
    if request.method == 'POST':
        form = forms.JadwalForm(request.POST)
        if form.is_valid():
            form.save()
            return redirect('homepage:jadwal')

    form = forms.JadwalForm()
    return render(request, 'schedule_create.html', {'form': form})

def schedule_delete(request):
	jadwal = Jadwal.objects.all().delete()
	return render(request, 'schedule.html')










